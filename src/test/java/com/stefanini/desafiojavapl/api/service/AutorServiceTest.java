package com.stefanini.desafiojavapl.api.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.stefanini.desafiojavapl.domain.exception.BusinessException;
import com.stefanini.desafiojavapl.domain.model.Autor;
import com.stefanini.desafiojavapl.domain.model.Obra;
import com.stefanini.desafiojavapl.domain.model.Sexo;
import com.stefanini.desafiojavapl.domain.repository.AutorRepository;
import com.stefanini.desafiojavapl.domain.service.Service;
import com.stefanini.desafiojavapl.domain.service.impl.AutorServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class AutorServiceTest {
	
	Service<Autor> autorService;
	
	@MockBean
    AutorRepository autorRepository;
	
	@BeforeEach
    public void setUp() {
        this.autorService = new AutorServiceImpl(autorRepository);
    }
	
	@Test
    @DisplayName("Deve lançar erro de negocio ao tentar salvar um autor com cpf duplicado")
    public void autorCpfDuplicadoTest(){
        Autor autor = criaValidoAutor();
        when(autorRepository.existsByCpf(Mockito.anyString())).thenReturn(true);

        Throwable exception = Assertions.catchThrowable(() -> autorService.adicionar(autor));

        assertThat(exception)
                .isInstanceOf(BusinessException.class)
                .hasMessage("Cpf já existente");

        Mockito.verify(autorRepository, Mockito.never()).save(autor);

    }
	
	@Test
    @DisplayName("Deve salvar um autor")
    public void salvaAutorTest() {
		LocalDate data = LocalDate.of(2020, 8, 1);

    	Obra obra= Obra.builder()
    	.id(1l)
    	.nome("Código Limpo")
    	.descricao("Desenvolvimento")
    	.dataPublicacao(data)
    	.dataExposicao(data)
    	.build();
    	
    	List<Obra> obraList = new ArrayList<Obra>();
    	obraList.add(obra);
    	
        Autor autor = criaValidoAutor();
        when(autorRepository.existsByCpf(Mockito.anyString())).thenReturn(false);
        when(autorRepository.existsByEmail(Mockito.anyString())).thenReturn(false);
        when(autorRepository.save(autor)).thenReturn(
        		Autor.builder()
	        		.id(1l)
	                .nome("Artur Rodrigues")
	                .sexo(Sexo.MASCULINO)
	                .paisOrigem("BR")
	                .cpf("11471487407")
	                .dataNascimento(data)
	                .email("artur_dgmr@hotmail.com")
	                .obras(obraList)
	                .build()
        );

        Autor autorSalvo = autorService.adicionar(autor);

        assertThat(autorSalvo.getId()).isNotNull();
        assertThat(autorSalvo.getNome()).isEqualTo("Artur Rodrigues");
        assertThat(autorSalvo.getCpf()).isEqualTo("11471487407");
        assertThat(autorSalvo.getEmail()).isEqualTo("artur_dgmr@hotmail.com");
    }
	
	@Test
    @DisplayName("Deve obter um autor por Id")
    public void obterPorIdTest(){
        Long id = 1l;
        Autor autor = criaValidoAutor();
        autor.setId(id);

        when(autorRepository.findById(id)).thenReturn(Optional.of(autor));

        Optional<Autor> procuraAutor = autorService.obterPorId(id);

        assertThat(procuraAutor.isPresent()).isTrue();
        assertThat(procuraAutor.get().getId()).isEqualTo(id);
    }
	
	@Test
    @DisplayName("Deve retornar vazio ao obter um autor por Id quando ele não existe na base.")
    public void autorNaoExisteTest(){
        Long id = 1l;
        when(autorRepository.findById(id)).thenReturn(Optional.empty());

        Optional<Autor> autor = autorService.obterPorId(id);

        assertThat(autor.isPresent()).isFalse();

    }
	
	@Test
    @DisplayName("Deve falhar ao deletar um autor com obras cadastradas")
    public void falhaDeletarAutorComObrasTest(){
		Autor autor = criaValidoAutor();

		org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class,
				() -> autorService.deletar(autor));

        Mockito.verify(autorRepository, Mockito.never()).delete(autor);
    }
	
	@Test
    @DisplayName("Deve deletar um autor.")
    public void deletaAutorTest(){
		Autor autor = criaAutorSemObra();
		
		System.out.println("Aquiiiiiiiii "+autor.toString());

        org.junit.jupiter.api.Assertions.assertDoesNotThrow(() -> autorService.deletar(autor));

        Mockito.verify(autorRepository, Mockito.times(1)).delete(autor);
    }
	
	private Autor criaValidoAutor() {
    	LocalDate data = LocalDate.of(2020, 8, 1);

    	Obra obra= Obra.builder()
    	.id(1l)
    	.nome("Código Limpo")
    	.descricao("Desenvolvimento")
    	.dataPublicacao(data)
    	.dataExposicao(data)
    	.build();
    	
    	List<Obra> obraList = new ArrayList<Obra>();
    	obraList.add(obra);
    	
        return Autor.builder()
                .id(1l)
                .nome("Artur Rodrigues")
                .sexo(Sexo.MASCULINO)
                .paisOrigem("BR")
                .cpf("11471487407")
                .dataNascimento(data)
                .email("artur_dgmr@hotmail.com")
                .obras(obraList)
                .build();
    }
	
	private Autor criaAutorSemObra() {
    	LocalDate data = LocalDate.of(2020, 8, 1);
    	
        return Autor.builder()
                .id(1l)
                .nome("Artur Rodrigues")
                .sexo(Sexo.MASCULINO)
                .paisOrigem("BR")
                .cpf("11471487407")
                .dataNascimento(data)
                .email("artur_dgmr@hotmail.com")
                .build();
    }
}

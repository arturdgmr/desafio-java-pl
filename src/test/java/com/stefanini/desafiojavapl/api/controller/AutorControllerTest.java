package com.stefanini.desafiojavapl.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stefanini.desafiojavapl.api.v1.model.AutorDTO;
import com.stefanini.desafiojavapl.api.v1.model.ObraDTO;
import com.stefanini.desafiojavapl.domain.model.Autor;
import com.stefanini.desafiojavapl.domain.model.Obra;
import com.stefanini.desafiojavapl.domain.model.Sexo;
import com.stefanini.desafiojavapl.domain.service.Service;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyLong;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@SpringBootTest
public class AutorControllerTest {
	
	static String URL_AUTOR_API = "/api/autores";
	
	@Autowired
    MockMvc mockMvc;

    @MockBean
    Service<Autor> service;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    @DisplayName("Cadastra um novo autor")
    public void criaAutorTest() throws Exception{
        AutorDTO autorDTO = criaValidoAutorDTO();
        
        Obra obraDTO = Obra.builder()
            	.id(1l)
            	.nome("Código Limpo")
            	.descricao("Desenvolvimento")
            	.dataPublicacao(autorDTO.getDataNascimento())
            	.dataExposicao(autorDTO.getDataNascimento())
            	.build();
        
        List<Obra> obraList = new ArrayList<Obra>();
        obraList.add(obraDTO);
        
        Autor savedAutor = criaValidoAutor();

        BDDMockito.given(service.adicionar(Mockito.any(Autor.class))).willReturn(savedAutor);

        mockMvc.perform(post(URL_AUTOR_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(autorDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty());
    }
    
    @Test
    @DisplayName("Deve lançar erro de validação quando não houver dados suficiente para criação do autor")
    public void erroAutorNomeTest() throws Exception{
        
        String json = new ObjectMapper().writeValueAsString(new AutorDTO());

        mockMvc.perform(post(URL_AUTOR_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", hasSize(3)));
    }
    
    @Test
    @DisplayName("Deve obter informacoes de um autor.")
    public void retornaAutorDetailsTest() throws Exception{
        Long id = 1l;
        
        AutorDTO autorDTO = criaValidoAutorDTO();
        
        Autor autor = Autor.builder()
                    .id(id)
                    .nome(autorDTO.getNome())
                    .sexo(autorDTO.getSexo())
                    .paisOrigem(autorDTO.getPaisOrigem())
                    .cpf(autorDTO.getCpf())
                    .dataNascimento(autorDTO.getDataNascimento())
                    .email(autorDTO.getEmail())
                    .build();

        BDDMockito.given( service.obterPorId(id) ).willReturn(Optional.of(autor));
        
        mockMvc.perform(get(URL_AUTOR_API.concat("/" + id))
            	.accept(MediaType.APPLICATION_JSON))
            	.andExpect(status().isOk())
	            .andExpect(jsonPath("id").value(id))
	            .andExpect(jsonPath("nome").value(autorDTO.getNome()))
	            .andExpect(jsonPath("sexo").value(autorDTO.getSexo().toString()))
	            .andExpect(jsonPath("paisOrigem").value(autorDTO.getPaisOrigem()))
	            .andExpect(jsonPath("cpf").value(autorDTO.getCpf()))
	            .andExpect(jsonPath("email").value(autorDTO.getEmail()));
    }
    
    @Test
    @DisplayName("Deve retornar resource not found quando o autor procurado não existir")
    public void autorNotFoundTest() throws Exception {

        BDDMockito.given(service.obterPorId(Mockito.anyLong())).willReturn(Optional.empty());

        mockMvc.perform(get(URL_AUTOR_API.concat("/" + 1))
            	.accept(MediaType.APPLICATION_JSON))
            	.andExpect(status().isNotFound());
    }
    
    @Test
    @DisplayName("Deve atualizar um autor")
    public void atualizaAutorTest() throws Exception {
        Long id = 1l;

        Autor atualizandoAutor = Autor
        		.builder()
        		.id(1l)
        		.cpf("111.111.111-11")
        		.dataNascimento(null)
        		.email("artur_dd@hotmail.com")
        		.nome("Artur")
        		.sexo(Sexo.MASCULINO)
        		.paisOrigem("BR")
        		.build();
        
        BDDMockito.given(service.obterPorId(id)).willReturn(Optional.of(atualizandoAutor));
        
        Autor autorAtualizado = Autor
        		.builder()
        		.id(1l)
        		.cpf("114.714.874-07")
        		.dataNascimento(null)
        		.email("artur_dgmr@hotmail.com")
        		.nome("Artur Rodrigues")
        		.sexo(Sexo.MASCULINO)
        		.paisOrigem("BR")
        		.build();
        BDDMockito.given(service.atualizar(atualizandoAutor)).willReturn(autorAtualizado);
        
        String json = new ObjectMapper().writeValueAsString(autorAtualizado);

        mockMvc.perform(put(URL_AUTOR_API.concat("/" + 1))
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk() )
                .andExpect(jsonPath("id").value(id))
                .andExpect(jsonPath("nome").value(autorAtualizado.getNome()))
                .andExpect(jsonPath("email").value(autorAtualizado.getEmail()))
                .andExpect(jsonPath("cpf").value(autorAtualizado.getCpf()));
    }
    
    @Test
    @DisplayName("Deve deletar um autor")
    public void deletaAutorTest() throws Exception {

        BDDMockito.given(service.obterPorId(anyLong())).willReturn(Optional.of(Autor.builder().id(1l).build()));

        mockMvc.perform(delete(URL_AUTOR_API.concat("/" + 1)))
        .andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("Deve retornar resource not found quando não encontrar o autor para deletar")
    public void deletaInexistentAutorTest() throws Exception {

        BDDMockito.given(service.obterPorId(anyLong())).willReturn(Optional.empty());

        mockMvc.perform(delete(URL_AUTOR_API.concat("/" + 1)))
        .andExpect(status().isNotFound());
    }
    
    
    private Autor criaValidoAutor() {
    	LocalDate data = LocalDate.of(2020, 8, 1);

    	Obra obra= Obra.builder()
    	.id(1l)
    	.nome("Código Limpo")
    	.descricao("Desenvolvimento")
    	.dataPublicacao(data)
    	.dataExposicao(data)
    	.build();
    	
    	List<Obra> obraList = new ArrayList<Obra>();
    	obraList.add(obra);
    	
        return Autor.builder()
                .id(1l)
                .nome("Artur Rodrigues")
                .sexo(Sexo.MASCULINO)
                .paisOrigem("BR")
                .cpf("11471487407")
                .dataNascimento(data)
                .email("artur_dgmr@hotmail.com")
                .obras(obraList)
                .build();
    }
    
    private AutorDTO criaValidoAutorDTO() {
    	LocalDate data = LocalDate.of(2020, 8, 1);
    	
    	ObraDTO obra= ObraDTO.builder()
    	.id(1l)
    	.nome("Código Limpo")
    	.descricao("Desenvolvimento")
    	.dataPublicacao(data)
    	.dataExposicao(data)
    	.build();
    	
    	List<ObraDTO> obraList = new ArrayList<ObraDTO>();
    	obraList.add(obra);
    	
        return AutorDTO
                .builder()
                .id(1l)
                .nome("Artur Rodrigues")
                .sexo(Sexo.MASCULINO)
                .paisOrigem("BR")
                .cpf("11471487407")
                .dataNascimento(data)
                .email("artur_dgmr@hotmail.com")
                .obras(obraList)
                .build();
    }

}

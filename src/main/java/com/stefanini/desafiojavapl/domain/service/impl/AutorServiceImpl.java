package com.stefanini.desafiojavapl.domain.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.stefanini.desafiojavapl.domain.exception.BusinessException;
import com.stefanini.desafiojavapl.domain.model.Autor;
import com.stefanini.desafiojavapl.domain.service.Service;
import com.stefanini.desafiojavapl.domain.repository.AutorRepository;

import org.springframework.data.domain.*;

@org.springframework.stereotype.Service
public class AutorServiceImpl implements Service<Autor>{
	
	private AutorRepository autorRepository;
	
	public AutorServiceImpl(AutorRepository autorRepository) {
        this.autorRepository = autorRepository;
    }

	@Override
	@Transactional
	public Autor adicionar(Autor autor) {
		if(autorRepository.existsByCpf(autor.getCpf())){
            throw new BusinessException("Cpf já existente");
        }else if(autorRepository.existsByEmail(autor.getEmail())) {
        	throw new BusinessException("Email já existente");
        }
        return autorRepository.save(autor);
	}

	@Override
	public Optional<Autor> obterPorId(Long id) {
		return autorRepository.findById(id);
	}

	@Override
	@Transactional
	public void deletar(Autor autor) {
		if(autor.getObras() != null){
            throw new IllegalArgumentException("Autor não pode ser excluido estando com obras");
        }
		autorRepository.delete(autor);
		
		
	}

	@Override
	@Transactional
	public Autor atualizar(Autor autor) {
		if(autor == null || autor.getId() == null){
            throw new IllegalArgumentException("Autor não pode ser atualizado");
        }
        return autorRepository.save(autor);
	}

	@Override
    public Page<Autor> obterPaginado(Autor filter, Pageable pageRequest) {
        Example<Autor> exemple = Example.of(filter,
                    ExampleMatcher
                            .matching()
                            .withIgnoreCase()
                            .withIgnoreNullValues()
                            .withStringMatcher( ExampleMatcher.StringMatcher.CONTAINING )
        ) ;
        return autorRepository.findAll(exemple, pageRequest);
    }

}

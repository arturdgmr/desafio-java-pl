package com.stefanini.desafiojavapl.domain.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.stefanini.desafiojavapl.domain.model.Obra;
import com.stefanini.desafiojavapl.domain.service.Service;
import com.stefanini.desafiojavapl.domain.repository.ObraRepository;

@org.springframework.stereotype.Service
public class ObraServiceImpl implements Service<Obra>{
	
	private ObraRepository obraRepository;
	
	public ObraServiceImpl(ObraRepository obraRepository) {
        this.obraRepository = obraRepository;
    }

	@Override
	@Transactional
	public Obra adicionar(Obra obra) {
        return obraRepository.save(obra);
	}

	@Override
	public Optional<Obra> obterPorId(Long id) {
		return obraRepository.findById(id);
	}

	@Override
	@Transactional
	public void deletar(Obra obra) {
		if(obra == null || obra.getId() == null){
            throw new IllegalArgumentException("Autor não pode ser atualizado");
        }
		obraRepository.delete(obra);
		
	}

	@Override
	@Transactional
	public Obra atualizar(Obra obra) {
		if(obra == null || obra.getId() == null){
            throw new IllegalArgumentException("Autor não pode ser atualizado");
        }
        return obraRepository.save(obra);
	}

	@Override
    public Page<Obra> obterPaginado(Obra filter, Pageable pageRequest) {
        Example<Obra> exemple = Example.of(filter,
                    ExampleMatcher
                            .matching()
                            .withIgnoreCase()
                            .withIgnoreNullValues()
                            .withStringMatcher( ExampleMatcher.StringMatcher.CONTAINING )
        ) ;
        return obraRepository.findAll(exemple, pageRequest);
    }

}

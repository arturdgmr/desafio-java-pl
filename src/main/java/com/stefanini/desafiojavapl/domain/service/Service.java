package com.stefanini.desafiojavapl.domain.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface Service<T> {
	
	T adicionar(T any);

    Optional<T> obterPorId(Long id);

    void deletar(T any);

    T atualizar(T any);

    Page<T> obterPaginado(T any, Pageable pageRequest);

}

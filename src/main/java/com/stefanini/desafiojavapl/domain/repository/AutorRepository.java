package com.stefanini.desafiojavapl.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stefanini.desafiojavapl.domain.model.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Long>{
	boolean existsByCpf(String cpf);
	
	boolean existsByEmail(String email);

}

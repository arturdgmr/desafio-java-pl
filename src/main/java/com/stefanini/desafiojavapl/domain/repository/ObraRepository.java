package com.stefanini.desafiojavapl.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stefanini.desafiojavapl.domain.model.Obra;

@Repository
public interface ObraRepository extends JpaRepository<Obra, Long>{

}

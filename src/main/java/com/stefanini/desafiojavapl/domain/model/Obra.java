package com.stefanini.desafiojavapl.domain.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Obra {
	
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	private String descricao;
	
	private LocalDate dataPublicacao;
	
	private LocalDate dataExposicao;
	
	
	@ManyToMany
	@JoinTable(name = "autor_obra", 
				joinColumns = @JoinColumn(name = "obra_id"),
				inverseJoinColumns = @JoinColumn(name = "autor_id"))
	private List<Autor> autores;
}

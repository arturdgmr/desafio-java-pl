package com.stefanini.desafiojavapl.domain.model;

public enum Sexo {
	
	MASCULINO,
	FEMININO;

}

package com.stefanini.desafiojavapl.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class FileUpload {
	
	private String fileName;
	private Long fileSize;
	private String filePath;
	private byte[] file;
	
	public FileUpload(String fileName, Long fileSize, String filePath) {
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.filePath = filePath;
	}

}

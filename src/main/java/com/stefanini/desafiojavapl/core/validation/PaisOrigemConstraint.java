package com.stefanini.desafiojavapl.core.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Constraint(validatedBy = PaisOrigemValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PaisOrigemConstraint {
	
	String message() default "Pais inválido";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    
}

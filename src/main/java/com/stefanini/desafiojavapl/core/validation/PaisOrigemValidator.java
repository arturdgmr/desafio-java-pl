package com.stefanini.desafiojavapl.core.validation;

import java.util.Set;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PaisOrigemValidator implements ConstraintValidator<PaisOrigemConstraint, String>{
	
	private static final Set<String> ISO_COUNTRIES = new HashSet<String>
    (Arrays.asList(Locale.getISOCountries()));
	
	@Override
    public void initialize(PaisOrigemConstraint contactNumber) {
    }

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ISO_COUNTRIES.contains(value);
	}

}

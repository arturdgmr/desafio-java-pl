package com.stefanini.desafiojavapl.core.validation;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.caelum.stella.validation.CPFValidator;

import br.com.caelum.stella.ValidationMessage;

public class CpfValidator implements ConstraintValidator<CpfConstraint, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		
		CPFValidator cpfValidator = new CPFValidator(); 
		List<ValidationMessage> erros = cpfValidator.invalidMessagesFor(value); 
		
		if(erros.size()>0) {
			return false;
		}else {
			return true;
		}
	}

}

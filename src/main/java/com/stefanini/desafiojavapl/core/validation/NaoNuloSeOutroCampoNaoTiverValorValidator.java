package com.stefanini.desafiojavapl.core.validation;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.stefanini.desafiojavapl.api.v1.model.ObraDTO;

public class NaoNuloSeOutroCampoNaoTiverValorValidator 
	implements ConstraintValidator<NaoNuloSeOutroCampoNaoTiverValorConstraint, ObraDTO>{
	
	@Override
    public void initialize(NaoNuloSeOutroCampoNaoTiverValorConstraint field) {
    }

	@Override
	public boolean isValid(ObraDTO value, ConstraintValidatorContext context) {
		try {
			if(value.getDataExposicao() == null && value.getDataPublicacao() == null) {
				return false;
			}else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}

package com.stefanini.desafiojavapl.core.validation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = NaoNuloSeOutroCampoNaoTiverValorValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NaoNuloSeOutroCampoNaoTiverValorConstraint {
	
	String message() default "Informe uma das seguintes datas! Publicação ou Exposição";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}

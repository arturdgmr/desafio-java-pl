package com.stefanini.desafiojavapl.core.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	AutenticacaoService AutenticacaoService;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(AutenticacaoService).passwordEncoder(new BCryptPasswordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		/*.antMatchers(HttpMethod.POST, "/api/autores").permitAll()
		.antMatchers(HttpMethod.POST, "/api/bucket/*").permitAll()
		.antMatchers(HttpMethod.POST, "/api/file/upload/*").permitAll()
		.anyRequest().authenticated()
		.and().formLogin();*/
		.anyRequest().permitAll();
		http.cors();
		http.csrf().disable();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	}
}

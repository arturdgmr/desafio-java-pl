package com.stefanini.desafiojavapl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.stefanini.desafiojavapl")
@EntityScan("com.stefanini.desafiojavapl.domain.model")
@EnableJpaRepositories("com.stefanini.desafiojavapl.domain.repository")
public class DesafioJavaPlApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioJavaPlApplication.class, args);
	}

}

package com.stefanini.desafiojavapl.api.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.validation.UnexpectedTypeException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;

import com.stefanini.desafiojavapl.domain.exception.BusinessException;

import lombok.Getter;

public class ApiErros {
	@Getter private List<String> errors;

    public ApiErros(List<ObjectError> allErrors) {
        this.errors = new ArrayList<String>();
        allErrors.forEach((e) -> this.errors.add(e.getDefaultMessage()));
    }

    public ApiErros(BusinessException ex) {
        this.errors = Arrays.asList(ex.getMessage());
    }
    
    public ApiErros(UnexpectedTypeException ex) {
        this.errors = Arrays.asList(ex.getMessage());
    }
    
    public ApiErros(EntityNotFoundException ex) {
        this.errors = Arrays.asList(ex.getMessage());
    }
    
    public ApiErros(HttpMessageNotReadableException ex) {
        this.errors = Arrays.asList(ex.getMessage());
    }
    
    public ApiErros(IllegalArgumentException ex) {
        this.errors = Arrays.asList(ex.getMessage());
    }
    
    public ApiErros(DataIntegrityViolationException ex) {
        this.errors = Arrays.asList("Exceção de violação de integridade de dados: "
        		+ ex.getMessage());
    }
}

package com.stefanini.desafiojavapl.api.v1.model;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.stefanini.desafiojavapl.core.validation.CpfConstraint;
import com.stefanini.desafiojavapl.core.validation.PaisOrigemConstraint;
import com.stefanini.desafiojavapl.domain.model.Sexo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AutorDTO {
	
	private Long id;
	
	@NotBlank(message = "Nome é obrigatório")
	private String nome;
	
	private Sexo sexo;
	
	@Email(message = "Email deve ser válido")
	private String email;
	
	@Past(message = "Data de nascimento inválida")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataNascimento;
	
	@CpfConstraint
	private String cpf;
	
	@PaisOrigemConstraint
	private String paisOrigem;
	
	@JsonIgnoreProperties("autores")
	private List<ObraDTO> obras;
	
	
}

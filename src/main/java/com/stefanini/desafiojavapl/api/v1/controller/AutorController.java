package com.stefanini.desafiojavapl.api.v1.controller;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.stefanini.desafiojavapl.api.v1.model.AutorDTO;
import com.stefanini.desafiojavapl.domain.model.Autor;
import com.stefanini.desafiojavapl.domain.model.Obra;
import com.stefanini.desafiojavapl.domain.service.Service;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/autores")
public class AutorController {
	
	private ModelMapper modelMapper;
	private Service<Autor> service;
	
	public AutorController(Service<Autor> service, ModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }
	
	@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AutorDTO adicionarAutor(@RequestBody @Valid AutorDTO autorDTO){
		Autor autor = modelMapper.map(autorDTO, Autor.class);
		autor = service.adicionar(autor);
        return modelMapper.map(autor, AutorDTO.class);
    }
	
	@GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AutorDTO obterAutorPorId(@PathVariable Long id){
        return service.obterPorId(id)
                .map(autor -> modelMapper.map(autor, AutorDTO.class))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
	
	@DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAutor(@PathVariable Long id){
        Autor autor = service.obterPorId(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        service.deletar(autor);
    }
	
	@PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AutorDTO atualizarAutor(@PathVariable Long id, @RequestBody AutorDTO autorDTO){
		Autor autor1 = modelMapper.map(autorDTO, Autor.class);
		List<Obra> obraList = autor1.getObras();
    	
        return service.obterPorId(id).map((autor) -> {
            autor.setCpf((autorDTO.getCpf()));
            autor.setDataNascimento(autorDTO.getDataNascimento());
            autor.setEmail(autorDTO.getEmail());
            autor.setNome(autorDTO.getNome());
            autor.setSexo(autorDTO.getSexo());
            autor.setObras(obraList);
            service.atualizar(autor);
            return modelMapper.map(autor, AutorDTO.class);
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
	
	@GetMapping
    public Page<AutorDTO> obterAutorPaginado(AutorDTO dto, Pageable pageable){
		Autor autor = modelMapper.map(dto, Autor.class);
        Page<Autor> resultado = service.obterPaginado(autor, pageable);
        List<AutorDTO> lista = resultado.getContent()
                .stream()
                .map(entity -> modelMapper.map(entity, AutorDTO.class))
                .collect(Collectors.toList());

        return new PageImpl<AutorDTO>( lista, pageable, resultado.getTotalElements() );
    }
	
	

}

package com.stefanini.desafiojavapl.api.v1.model;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.stefanini.desafiojavapl.core.validation.NaoNuloSeOutroCampoNaoTiverValorConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@NaoNuloSeOutroCampoNaoTiverValorConstraint
public class ObraDTO {
	
	private Long id;
	
	@NotBlank
	private String nome;
	
	private String descricao;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataPublicacao;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataExposicao;
	
	@JsonIgnoreProperties("obras")
	@NotEmpty(message = "Autor não pode estar vazio")
	private List<AutorDTO> autores;
	
	

}

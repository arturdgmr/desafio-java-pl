package com.stefanini.desafiojavapl.api.v1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.stefanini.desafiojavapl.domain.model.FileUpload;
import com.stefanini.desafiojavapl.domain.service.FileUploadService;

@RestController
@RequestMapping(value = "/api")
public class FileUploadController {

	@Autowired
	private FileUploadService fileUploadService;

	@PostMapping(value = "/bucket/create/{bucketName}")
	public String createBucket(@PathVariable String bucketName) {
		return fileUploadService.createBucket(bucketName);
	}
	
	@PostMapping(value = "/file/upload/{bucketName}")
	public String fileUplaod(@PathVariable String bucketName, MultipartFile file) {
		return fileUploadService.fileUplaod(bucketName, file);
	}

	@GetMapping(value = "/bucket/list")
	public List<String> getBucketList() {
		return fileUploadService.getBucketList();
	}

	@GetMapping(value = "/bucket/files/{bucketName}")
	public List<FileUpload> getBucketfiles(@PathVariable String bucketName) {
		return fileUploadService.getBucketfiles(bucketName);
	}
	@DeleteMapping(value = "/file/delete/{bucketName}/{fileName}")
	public String deleteFile(@PathVariable String bucketName, @PathVariable String fileName) {
		return fileUploadService.deleteFile(bucketName, fileName);
	}

}

package com.stefanini.desafiojavapl.api.v1.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.stefanini.desafiojavapl.api.v1.model.ObraDTO;
import com.stefanini.desafiojavapl.domain.model.Obra;
import com.stefanini.desafiojavapl.domain.service.Service;

@RestController
@RequestMapping("/api/obras")
public class ObraController {
	
	private ModelMapper modelMapper;
	private Service<Obra> service;
	
	public ObraController(Service<Obra> service, ModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }
	
	@PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ObraDTO adicionarAutor(@RequestBody @Valid ObraDTO obraDTO){
		Obra obra = modelMapper.map(obraDTO, Obra.class);
		obra = service.adicionar(obra);
        return modelMapper.map(obra, ObraDTO.class);
    }
	
	@GetMapping
    public Page<ObraDTO> obterAutorPaginado(ObraDTO dto, Pageable pageable){
		Obra obra = modelMapper.map(dto, Obra.class);
        Page<Obra> resultado = service.obterPaginado(obra, pageable);
        List<ObraDTO> lista = resultado.getContent()
                .stream()
                .map(entity -> modelMapper.map(entity, ObraDTO.class))
                .collect(Collectors.toList());

        return new PageImpl<ObraDTO>( lista, pageable, resultado.getTotalElements() );
    }
	
	@DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAutor(@PathVariable Long id){
		Obra obra = service.obterPorId(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        service.deletar(obra);
    }

}
